;(function ( $, window, document, undefined ) {
	$(document).on('click', '.js-toggle',function(e) {
		e.preventDefault();
		var $targ = $(this).data('target') ? $($(this).data('target')) : $(this),
			state = $(this).data('state').split(', ') || 'is-active';

		//Swap text
		if($(this).data('text')) {
			var swaptext = $(this).text();
		
			$(this)
				.text($(this).data('text'))
				.data('text', swaptext);
		}
		
		for (var i = state.length - 1; i >= 0; i--) {
			console.log(i, $targ[i], state[i], $targ);
			$targ.eq(i).toggleClass(state[i])
		};
		$targ.toggleClass(state);
		$targ.trigger('Mangrove:ToggledState');
	});
})( jQuery, window, document );